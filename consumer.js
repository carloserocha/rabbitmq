const amqp = require('amqplib/callback_api')
const mailer = require('./mailer')

const { sleep } = require('sleep')
const { AMQP_URL } = require('./config.json')

amqp.connect(AMQP_URL, (error, connection) => {
    if (error) throw new Error (error)

    createChannel (connection)
})

const createChannel = (connection) => {
    connection.createChannel ((err, channel) => {
        if (err) throw new Error (err)

        let queue = 'dede'
        channel.assertQueue(queue, { durable: true })
        channel.prefetch(1)

        consumer (channel, queue)
    })
}

const consumer = (channel, queue) => {
    channel.consume (queue, (msg) => {
        const object = JSON.parse(JSON.stringify(msg.content.toString()))
        mailer('trey.will@ethereal.email', object)
    }, { noAck: false })
    // noAck: nao remove o item da fila automaticamente quando processado
}