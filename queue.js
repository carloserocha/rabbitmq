const amqp = require('amqplib/callback_api')
const { AMQP_URL } = require('./config.json')

amqp.connect(AMQP_URL, (error, connection) => {
    if (error) throw new Error(error)

    channel_task (connection)
})

const channel_task = (connection) => {
    connection.createChannel((error, channel) => {
        if (error) throw new Error (error)

        let queue = 'foo'

        const msg = `Hello World!`

        channel.assertQueue(queue, { durable: true })
        channel.sendToQueue(queue, Buffer.from(msg), { persistent: true })
    })
}