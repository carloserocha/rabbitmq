const CONFIG = require('./config.json')
const mailer = require('nodemailer')

const send = (emailAddress, message) => {
    const transport = mailer.createTransport ({
        host: CONFIG.HOST,
        port: 587,
        secure: false,
        auth: {
            user: CONFIG.FROM,
            pass: CONFIG.PASSWORD
        }
    })

    const options = {
        from: CONFIG.FROM,
        to: emailAddress,
        subject: CONFIG.SUBJECT,
        text: message
    }
    transport.sendMail (options, (err, ok) => {
        if (err) throw new Error (err)

        console.log(ok, 'sucess')
    })
}

module.exports = send